export enum TodoAction {
  Add = "Add",
  Remove = "Remove",
  Complete = "Complete",
}

export const addTodo = (task: string) => ({ type: TodoAction.Add, data: task });
export const removeTodo = (id: string) => ({
  type: TodoAction.Remove,
  data: id,
});
export const completeTodo = (id: string) => ({
  type: TodoAction.Complete,
  data: id,
});

export type TodoActionType =
  | ReturnType<typeof addTodo>
  | ReturnType<typeof removeTodo>
  | ReturnType<typeof completeTodo>;
