import { TodoAction, TodoActionType } from "./actions";
import { ITodoState, IToDo } from "./interface";
import { v4 as uuidv4 } from "uuid";

export const todoReducer = (
  state: ITodoState,
  action: TodoActionType
): ITodoState => {
  switch (action.type) {
    case TodoAction.Add: {
      const todo: IToDo = {
        task: action.data,
        createdAt: new Date(),
        id: uuidv4(),
      };

      return { ...state, todos: [...state.todos, todo] };
    }

    case TodoAction.Remove: {
      const todoIndex = state.todos.findIndex(
        (todo) => todo.id === action.data
      );

      if (todoIndex > -1) {
        const updatedTodos = [...state.todos];

        updatedTodos.splice(todoIndex, 1);

        return { ...state, todos: updatedTodos };
      }

      return state;
    }

    case TodoAction.Complete: {
      const todoIndex = state.todos.findIndex(
        (todo) => todo.id === action.data
      );

      if (todoIndex > -1) {
        const updatedTodos = [...state.todos];

        updatedTodos[todoIndex].completed = true;
        updatedTodos[todoIndex].completedAt = new Date();

        return { ...state, todos: updatedTodos };
      }
      return state;
    }

    default:
      return state;
  }
};
