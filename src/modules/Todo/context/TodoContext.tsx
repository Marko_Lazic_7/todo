import React, { FC, useCallback, useContext, useMemo, useReducer } from "react";
import { addTodo, completeTodo, removeTodo } from "./actions";
import { ITodoState, TodoContextType } from "./interface";
import { todoReducer } from "./reducer";

const initialState: ITodoState = {
  todos: [],
  completedTodos: [],
  incompleteTodos: [],
};

const TodoContext = React.createContext({} as TodoContextType);

export const TodoContextProvider: FC = ({ children }) => {
  const [state, dispatch] = useReducer(todoReducer, initialState);

  const add = useCallback((task: string) => dispatch(addTodo(task)), []);
  const remove = useCallback((id: string) => dispatch(removeTodo(id)), []);
  const complete = useCallback((id: string) => dispatch(completeTodo(id)), []);

  const value = {
    ...state,
    add,
    remove,
    complete,
    incompleteTodos: state.todos.filter((todo) => !todo.completed),
    completedTodos: state.todos.filter((todo) => todo.completed),
  };

  return <TodoContext.Provider value={value}>{children}</TodoContext.Provider>;
};

export const useTodoContext = () => useContext(TodoContext);
