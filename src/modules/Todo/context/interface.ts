export interface IToDo {
  id: string;
  task: string;
  createdAt: Date;
  completed?: boolean;
  completedAt?: Date;
}

export interface ITodoState {
  todos: IToDo[];
  incompleteTodos: IToDo[];
  completedTodos: IToDo[];
}

export interface ITodoActions {
  add: (task: string) => void;
  remove: (id: string) => void;
  complete: (id: string) => void;
}

export type TodoContextType = ITodoState & ITodoActions;
