import { CreateTodo } from "./components/CreateTodo";
import { List } from "./components/List";
import styles from "./styles.module.css";

export const TodoView = () => {
  return (
    <div className={styles.root}>
      <div className={styles.lists}>
        <List />
      </div>
      <div className={styles.form}>
        <CreateTodo />
      </div>
    </div>
  );
};
