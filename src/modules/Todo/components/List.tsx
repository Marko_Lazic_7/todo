import { FC } from "react";
import { useTodoContext } from "../context/TodoContext";
import { ListItem } from "./ListItem";
import styles from "./styles.module.css";

export const List: FC = () => {
  const { incompleteTodos, completedTodos } = useTodoContext();

  return (
    <div className={styles.list}>
      <div>
        <h3>Todos</h3>
        <div>
          {incompleteTodos.length > 0 ? (
            incompleteTodos.map((todo) => (
              <div key={todo.id}>
                <ListItem item={todo} />
              </div>
            ))
          ) : (
            <div>Todo list is empty.</div>
          )}
        </div>
      </div>
      <div>
        <h3>Completed</h3>
        <div>
          {completedTodos.length > 0 ? (
            completedTodos.map((todo) => (
              <div key={todo.id}>
                <ListItem item={todo} />
              </div>
            ))
          ) : (
            <div>Completed list is empty.</div>
          )}
        </div>
      </div>
    </div>
  );
};
