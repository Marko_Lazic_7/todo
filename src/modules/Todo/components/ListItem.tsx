import { FC } from "react";
import { Button } from "ui/Button/Component";
import { IToDo } from "../context/interface";
import { useTodoContext } from "../context/TodoContext";
import styles from "./styles.module.css";

interface IListItemProps {
  item: IToDo;
}

export const ListItem: FC<IListItemProps> = ({ item }) => {
  const { complete, remove } = useTodoContext();

  return (
    <div className={styles.listItem}>
      <div className={styles.listItemTodoWrapper}>
        <div>
          <span>{item.task}</span>
        </div>
        <div className={styles.listItemMeta}>
          <span>
            <b>created at:</b>
            {item.createdAt.toISOString()}
          </span>
          {item.completed && (
            <span>
              <b>completed at:</b>
              {item.completedAt?.toISOString()}
            </span>
          )}
        </div>
      </div>
      <div className={styles.listItemActions}>
        {!item.completed && (
          <Button onClick={() => complete(item.id)}>Complete</Button>
        )}
        <Button onClick={() => remove(item.id)} variant="secondary">
          Remove
        </Button>
      </div>
    </div>
  );
};
