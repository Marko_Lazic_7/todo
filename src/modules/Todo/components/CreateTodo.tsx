import React, { FC, useState } from "react";
import { Button } from "ui/Button/Component";
import { TextField } from "ui/TextField/Component";
import { useTodoContext } from "../context/TodoContext";

export const CreateTodo: FC = () => {
  const { add } = useTodoContext();
  const [task, setTask] = useState("");

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setTask(e.currentTarget.value);

  const onAdd = () => {
    if (task.length > 3) {
      add(task);
      setTask("");
    } else {
      alert("Please properly describe the task");
    }
  };

  return (
    <div>
      <h3>Add a todo</h3>
      <TextField
        value={task}
        onChange={onChange}
        placeholder="Describe a task here..."
        style={{ width: 250 }}
        onKeyPress={(e) => (e.key === "Enter" ? onAdd() : undefined)}
      />
      <Button onClick={onAdd}>Add</Button>
    </div>
  );
};
