import { ComponentPropsWithoutRef, FC } from "react";
import cx from "classnames";
import styles from "./styles.module.css";

interface IButtonProps extends ComponentPropsWithoutRef<"button"> {
  variant?: "primary" | "secondary";
}

export const Button: FC<IButtonProps> = ({
  children,
  variant = "primary",
  className,
  ...props
}) => {
  const btnClass = cx(
    styles.root,
    { [styles.secondary]: variant === "secondary" },
    className
  );
  return (
    <button className={btnClass} {...props}>
      {children}
    </button>
  );
};
