import { ComponentPropsWithoutRef, FC } from "react";
import cx from "classnames";
import styles from "./styles.module.css";

interface ITextFieldProps
  extends Omit<ComponentPropsWithoutRef<"input">, "type"> {}

export const TextField: FC<ITextFieldProps> = ({ className, ...props }) => {
  return (
    <input type="text" className={cx(styles.root, className)} {...props} />
  );
};
